export default () => ([
  {
    id: `${Math.random() * 41}`,
    imageSource: `https://images.pexels.com/photos/671557/pexels-photo-671557.jpeg?w=4000&buster=${Math.random()}`,
    thumbnailSource: `https://images.pexels.com/photos/671557/pexels-photo-671557.jpeg?w=50&buster=${Math.random()}`,
    name: 'Rex',
    comments: 'Very lovable dog',
    species: 'Dog',
    breed: 'Beagle',
    colour: 'Brown',
    status: 'LOST',
    date: 'Date',
    location: {
      lat: 0,
      long: 0
    },
    contactDetails: {
      name: 'James',
      number: '011-951-7865',
      email: 'james.bond@email.com'
    }
  },
  {
    id: `${Math.random() * 97}`,
    imageSource: `https://images.pexels.com/photos/671556/pexels-photo-671556.jpeg?w=4000&buster=${Math.random()}`,
    thumbnailSource: `https://images.pexels.com/photos/671556/pexels-photo-671556.jpeg?w=50&buster=${Math.random()}`,
    comments: 'Cute cat',
    species: 'Cat',
    breed: 'Persian',
    colour: 'White',
    status: 'FOUND',
    date: 'Date',
    location: {
      lat: 0,
      long: 0
    },
    contactDetails: {
      name: 'Karen',
      number: '011-741-1825',
      email: 'karen.daisy@email.com'
    }
  },
  {
    id: `${Math.random() * 171}`,
    imageSource: `https://images.pexels.com/photos/671555/pexels-photo-671555.jpeg?w=4000&buster=${Math.random()}`,
    thumbnailSource: `https://images.pexels.com/photos/671555/pexels-photo-671555.jpeg?w=50&buster=${Math.random()}`,
    name: 'Bubbles',
    comments: 'Loves the phrase "Who wants a treat?"',
    species: 'Dog',
    breed: 'Bulldg',
    colour: 'Grey',
    status: 'LOST',
    date: 'Date',
    location: {
      lat: 0,
      long: 0
    },
    contactDetails: {
      name: 'Kevin',
      number: '011-399-6535',
      email: 'kevin.stone@email.com'
    }
  }
]);
