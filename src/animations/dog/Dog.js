import React from 'react';
import LottieView from 'lottie-react-native';
import PropTypes from 'prop-types';

const Dog = (props) => {
  const { onAnimationFinish } = props;
  return (
    <LottieView
      // eslint-disable-next-line global-require
      source={require('./dog.json')}
      autoPlay
      onAnimationFinish={() => onAnimationFinish()}
      enableMergePathsAndroidForKitKatAndAbove
    />
  );
};

Dog.propTypes = {
  onAnimationFinish: PropTypes.func,
};

Dog.defaultProps = {
  onAnimationFinish: () => null,
};

export default Dog;
