import Dog from './dog/Dog';
import FadeInView from './fades/FadeInView';

export {
  Dog,
  FadeInView,
};
