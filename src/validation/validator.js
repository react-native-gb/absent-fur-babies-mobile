import {
  EMAIL,
  PASSWORD, STRING
} from './fields';

const validator = require('validator');

// Strings
const isEmailValid = (value) => validator.isEmail(value);
const isEmptyString = (value) => value.length === 0;
const isPasswordPassword = (value) => value.toLowerCase() === 'password';
const isValidPasswordLength = (value) => value.length < 6;

// Numbers
const isZero = (value) => value === 0;
const isNegative = (value) => value < 0;

// eslint-disable-next-line consistent-return,import/prefer-default-export
export const Validate = ({ field, value }) => {
  switch (typeof value) {
    case 'string':
      return ValidateStrings({ field, value });
    case 'number':
      console.warn('Number validation not yet implemented');
      break;
    case 'boolean':
      console.warn('Boolean validation not yet implemented');
      break;
    default:
      console.warn('Invalid validation type');
      return {
        isValid: false,
      };
  }
};

const ValidateStrings = ({ field, value }) => {
  switch (field) {
    case EMAIL:
      return ValidateEmail(value);
    case PASSWORD:
      return ValidatePassword(value);
    case STRING:
      return ValidateString(value);
    default:
      console.warn('Invalid validation field');
      return {
        isValid: false,
      };
  }
};

const ValidatePassword = (value) => {
  if (isEmptyString(value)) {
    return {
      isValid: false,
      message: 'Please enter a password'
    };
  }

  if (isPasswordPassword(value)) {
    return {
      isValid: false,
      message: 'Password cannot be password'
    };
  }

  if (isValidPasswordLength(value)) {
    return {
      isValid: false,
      message: 'Your password must be at least 7 characters'
    };
  }

  return {
    isValid: true
  };
};
const ValidateEmail = (value) => {
  if (isEmptyString(value)) {
    return {
      isValid: false,
      message: 'Please enter an email address'
    };
  }

  if (!isEmailValid(value)) {
    return {
      isValid: false,
      message: 'Please enter a valid email address'
    };
  }

  return {
    isValid: true,
  };
};

const ValidateString = (value) => {
  if (isEmptyString(value)) {
    return {
      isValid: false,
      message: 'Please enter a value'
    };
  }

  return {
    isValid: true
  };
};
