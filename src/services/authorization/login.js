import axios from 'axios';
import { BASE_SERVICE_URL_LOCAL } from 'react-native-dotenv';

const login = async ({ email, password }) => axios.post(
  `${BASE_SERVICE_URL_LOCAL}/users/login`,
  {
    email,
    password
  },
  {
    headers: {
      Accept: 'application/json'
    }
  }
);

export default login;
