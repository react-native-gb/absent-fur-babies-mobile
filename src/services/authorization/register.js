import axios from 'axios';
import { BASE_SERVICE_URL_LOCAL } from 'react-native-dotenv';

const register = async ({ name, email, password }) => axios.post(
  `${BASE_SERVICE_URL_LOCAL}/users`,
  {
    name,
    email,
    password
  },
  {
    headers: {
      Accept: 'application/json'
    }
  }
);

export default register;
