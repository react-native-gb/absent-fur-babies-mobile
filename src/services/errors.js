const errors = (code) => {
  let message = '';
  switch (code) {
    case 400:
      message = 'We think you forgot something?';
      // TODO: manage duplicate email error
      break;
    case 401:
      message = 'You shall not pass!';
      break;
    case 403:
      message = 'You shall not pass!';
      break;
    case 404:
      message = 'That was not the droid you were looking for';
      break;
    default:
      message = 'Our elves have broken something';
  }

  return message;
};

export default errors;
