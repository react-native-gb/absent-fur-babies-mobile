import cards from '../../../data/cards';

export default async () => (
  new Promise((res, rej) => {
    setTimeout(() => {
      res(cards());
    }, 500);
  })
);
