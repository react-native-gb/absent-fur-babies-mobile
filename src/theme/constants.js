const montserratRegular = 'Montserrat-Regular';
const montserratMedium = 'Montserrat-Medium';
const montserratBold = 'Montserrat-Bold';

const primary = 'primary';
const secondary = 'secondary';
const tertiary = 'tertiary';

const themeStore = '@reducer';
const activeTab = '#e91e63';

module.exports = {
  themeStore,
  primary,
  secondary,
  tertiary,
  montserratRegular,
  montserratMedium,
  montserratBold,
  activeTab,
};
