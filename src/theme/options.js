import {
  montserratRegular,
  montserratBold,
  montserratMedium,
  primary,
} from './constants';

const themeObject = {
  pri100: '#779ECC',
  pri75: '#92B1D6',
  pri50: '#ADC5E0',
  pri25: '#C9D8EB',

  sec100: '#9FC0DE',
  sec75: '#B2CDE5',
  sec50: '#C5D9EB',
  sec25: '#D9E6F2',

  ter100: '#F2C894',
  ter75: '#F5D3A9',
  ter50: '#F7DEBF',
  ter25: '#FAE9D4',

  quad100: '#FFB347',
  quad75: '#FFC26C',
  quad50: '#FFD191',
  quad25: '#FFE1B5',

  pent100: '#FF985A',
  pent75: '#FFAD7B',
  pent50: '#FFC19C',
  pent25: '#FFD6BD',

  grey100: '#9EA2AA',
  grey75: '#ACACAC',
  grey50: '#D7DDDD',
  grey25: '#DBDCD4',

  error: '#F8665F',
  white: '#f2f0e6',
  whiteTransparent: 'rgba(255,255,255,0.18)',

  greyTransparent: 'rgba(215,215,215,0.18)',

  codGrey: '#111111',
  athensGrey: '#e1e4e8',

  black: '#0d0f19',

  fontRegular: montserratRegular,
  fontMedium: montserratMedium,
  fontBold: montserratBold,
  fontSizeHeading: 34,
  fontSizeContext: 18,
  fontSizeButton: 14,
  fontSizeError: 12,
};

const themes = {
  [primary]: {
    pri100: '#779ECC',
    pri75: '#92B1D6',
    pri50: '#ADC5E0',
    pri25: '#C9D8EB',

    sec100: '#9FC0DE',
    sec75: '#B2CDE5',
    sec50: '#C5D9EB',
    sec25: '#D9E6F2',

    ter100: '#F2C894',
    ter75: '#F5D3A9',
    ter50: '#F7DEBF',
    ter25: '#FAE9D4',

    quad100: '#FFB347',
    quad75: '#FFC26C',
    quad50: '#FFD191',
    quad25: '#FFE1B5',

    pent100: '#FF985A',
    pent75: '#FFAD7B',
    pent50: '#FFC19C',
    pent25: '#FFD6BD',

    grey100: '#9EA2AA',
    grey75: '#ACACAC',
    grey50: '#D7DDDD',
    grey25: '#DBDCD4',

    error: '#F8665F',
    white: '#f2f0e6',
    whiteTransparent: 'rgba(255,255,255,0.18)',

    greyTransparent: 'rgba(215,215,215,0.18)',

    fontRegular: montserratRegular,
    fontMedium: montserratMedium,
    fontBold: montserratBold,
    fontSizeHeading: 34,
    fontSizeSubHeading: 24,
    fontSizeContext: 18,
    fontSizeButton: 14,
    fontSizeError: 12,
  }
};

export {
  themes,
  themeObject
};
