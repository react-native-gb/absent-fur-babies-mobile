import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import Background from '../../components/background/Background';
import Avatar from '../../components/icons/Avatar';
import { Content, Heading } from '../../typography';
import SquareIconButton from '../../components/buttons/SquareIconButton';
import LogoutIcon from '../../components/icons/Logout';
import { ThemeSelector } from '../../components';
import { logoutAction } from '../../redux/modules/authorization/actions';

const Settings = (props) => {
  const { theme, logout, navigation } = props;

  return (
    <Background>
      <View style={styles.container}>
        <View style={styles.headingWrapper}>
          {/* TODO: @Gareth */}
          <Avatar size={80} />
          <Heading>Hi Gareth</Heading>
          <Content>test@email.com</Content>
        </View>
        <View style={styles.bodyWrapper}>
          {/* TODO: @Gareth */}
          <ThemeSelector
            onPress={() => null}
          />
          <SquareIconButton
            style={{
              backgroundColor: theme.quad75,
            }}
            onPress={() => logout({ navigation })}
            icon={<LogoutIcon size={32} color={theme.white} />}
          >
            Logout
          </SquareIconButton>
        </View>
      </View>

    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
  },
  headingWrapper: {
    flex: 0.4,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 24
  },
  bodyWrapper: {
    flex: 0.6,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
});


SquareIconButton.propTypes = {
  // theme: PropTypes.shape({}).isRequired,
  logout: PropTypes.func,
};

SquareIconButton.defaultProps = {
  logout: () => null
};


const mapDispatchToProps = (dispatch) => ({
  logout: ({ navigation }) => dispatch(logoutAction({ navigation }))
});

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
