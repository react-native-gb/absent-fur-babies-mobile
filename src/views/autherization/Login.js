import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { connect } from 'react-redux';
// import PropTypes from 'prop-types';

// Components
import {
  Background,
  RoundedButton,
  EmailIconInput,
  PasswordIconInput,
  ToggleWithLabel
} from '../../components';
import { Dog } from '../../animations';
import {
  Heading
} from '../../typography';

// Actions
import {
  loginAction
} from '../../redux/modules/authorization/actions';
import TextButton from '../../components/buttons/TextButton';
import { REGISTRATION_ROUTE } from '../../navigation/routeNames';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      // eslint-disable-next-line react/no-unused-state
      canLogin: false,
      persistLogin: false,
    };
  }

  handleTextChange = ({ field, value }) => {
    this.setState({
      [field]: value,
    });
  };

  handleCanLogin = (validity) => {
    const { email, password } = this.state;
    const nonEmptyFields = email.length !== 0 && password.length !== 0;
    this.setState({
      // eslint-disable-next-line react/no-unused-state
      canLogin: validity && nonEmptyFields,
    });
  };

  handleLogin = () => {
    const { email, password, persistLogin } = this.state;
    const { login, navigation } = this.props;
    login({
      email, password, navigation, persistLogin
    });
  };

  handleLoginPersistence = (persist) => {
    this.setState({
      persistLogin: persist,
    });
  };

  render() {
    const { error, loading, navigation } = this.props;
    const {
      email, password,
    } = this.state;

    return (
      <Background>
        <View style={styles.container}>
          <View style={styles.headingWrapper}>
            <Dog />
            <Heading>
              Welcome Back
            </Heading>
          </View>
          <View style={styles.bodyWrapper}>
            <EmailIconInput
              placeholder="email"
              value={email.toLowerCase()}
              onTextChange={(value) => this.handleTextChange({ field: 'email', value })}
              isValid={(validity) => this.handleCanLogin(validity)}
            />
            <PasswordIconInput
              placeholder="password"
              value={password}
              onTextChange={(value) => this.handleTextChange({ field: 'password', value })}
              isValid={(validity) => this.handleCanLogin(validity)}
            />
            <RoundedButton
              // disabled={!canLogin}
              onPress={() => this.handleLogin()}
              error={error}
              loading={loading}
            >
              Login
            </RoundedButton>
            <ToggleWithLabel
              label="keep me logged in"
              onChange={this.handleLoginPersistence}
            />
            <TextButton
              onPress={() => navigation.navigate(REGISTRATION_ROUTE)}
            >
              Opps not yet registered?
            </TextButton>
          </View>
        </View>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
  },
  headingWrapper: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  bodyWrapper: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

Login.propTypes = {
};

const mapStateToProps = (state) => ({
  loading: state.authorization.loading.login,
  error: state.authorization.errors.login,
});

const mapDispatchToProps = (dispatch) => ({
  login: ({
    email, password, navigation, persistLogin
  }) => dispatch(loginAction({
    email, password, navigation, persistLogin
  }))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
