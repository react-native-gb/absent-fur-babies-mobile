import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';

import {
  Background,
  RoundedButton,
  EmailIconInput,
  PasswordIconInput,
  UserIconInput,
  FacebookIcon,
  TwitterIcon,
  GoogleIcon,
  IconRow
} from '../../components';
import {
  Content,
  Heading
} from '../../typography';
import SquareFullButton from '../../components/buttons/SquareFullButton';
import { Dog } from '../../animations';

// Actions
import {
  registerAction
} from '../../redux/modules/authorization/actions';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      // eslint-disable-next-line react/no-unused-state
      canCreateAccount: false,
    };
  }

  handleTextChange = ({ field, value }) => {
    this.setState({
      [field]: value,
    });
  };

  handleCanCreateAccount = (validity) => {
    const { email, password, name } = this.state;
    const nonEmptyFields = email.length !== 0 && password.length !== 0 && name.length !== 0;
    this.setState({
      // eslint-disable-next-line react/no-unused-state
      canCreateAccount: validity && nonEmptyFields,
    });
  };

  handleRegister = () => {
    const { email, password, name } = this.state;
    const { register, navigation } = this.props;
    register({
      email, password, name, navigation
    });
  };

  render() {
    const {
      navigation,
      error,
      loading,
      canCreateAccount,
    } = this.props;
    const {
      email, password, name,
    } = this.state;

    return (
      <Background>
        <View style={styles.container}>
          <View style={styles.headingWrapper}>
            <Dog />
            <Heading>
              Welcome Home
            </Heading>
          </View>
          <View style={styles.bodyWrapper}>
            <UserIconInput
              placeholder="name"
              value={name}
              onTextChange={(value) => this.handleTextChange({ field: 'name', value })}
              isValid={(validity) => this.handleCanCreateAccount(validity)}
            />
            <EmailIconInput
              placeholder="email"
              value={email.toLowerCase()}
              onTextChange={(value) => this.handleTextChange({ field: 'email', value })}
              isValid={(validity) => this.handleCanCreateAccount(validity)}
            />
            <PasswordIconInput
              placeholder="password"
              value={password}
              onTextChange={(value) => this.handleTextChange({ field: 'password', value })}
              isValid={(validity) => this.handleCanCreateAccount(validity)}
            />
            <RoundedButton
              disabled={!canCreateAccount}
              onPress={() => this.handleRegister()}
              error={error}
              loading={loading}
            >
              CREATE MY ACCOUNT
            </RoundedButton>
            <Content
              style={{
                marginBottom: 30,
              }}
            >
              or login with
            </Content>
            <IconRow>
              <TwitterIcon />
              <FacebookIcon />
              <GoogleIcon />
            </IconRow>
            <SquareFullButton
              onPress={() => navigation.navigate('Login')}
            >
              IM ALREADY A HERO
            </SquareFullButton>
          </View>
        </View>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
  },
  headingWrapper: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  bodyWrapper: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

const mapStateToProps = (state) => ({
  loading: state.authorization.loading.register,
  error: state.authorization.errors.register,
});

const mapDispatchToProps = (dispatch) => ({
  register:
    ({
      name, email, password, navigation
    }) => dispatch(registerAction({
      name, email, password, navigation
    }))
});

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
