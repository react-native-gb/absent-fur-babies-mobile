import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet, View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { connect } from 'react-redux';
import {
  Background, Cards
} from '../../components';
import * as cardTypes from '../../components/cards/constants/CardTypes';
import { fetchDataAction } from '../../redux/modules/dashboard/actions';
import { Dog } from '../../animations';
import { Heading } from '../../typography';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.fetchDashboardData();
  }

  fetchDashboardData = () => {
    const { loading, fetchData } = this.props;
    if (loading) { return; }
    fetchData();
  };

  onScroll = ({ nativeEvent }) => {
    const { layoutMeasurement, contentOffset, contentSize } = nativeEvent;
    if (layoutMeasurement.height + contentOffset.y >= contentSize.height - 50) {
      this.fetchDashboardData();
    }
  };

  render() {
    const {
      loading,
      cards,
    } = this.props;

    return (
      <Background>
        <View style={styles.container}>
          <View style={styles.headingWrapper}>
            <Dog />
            <Heading>
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              You'll Find Them!
            </Heading>
          </View>

          <View style={styles.bodyWrapper}>
            <ScrollView
              nestedScrollEnabled
              onScroll={this.onScroll}
            >
              <Cards
                type={cardTypes.ADVERT_CARD}
                items={cards}
              />
            </ScrollView>
            {/* TODO: @Gareth */}
            <ActivityIndicator animating={loading} />
          </View>
        </View>
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
  },
  headingWrapper: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 16
  },
  bodyWrapper: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

Dashboard.propTypes = {
  loading: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  cards: PropTypes.array.isRequired,
  navigation: PropTypes.shape({}).isRequired,
  // token: PropTypes.string,
};

Dashboard.defaultProps = {
  loading: false,
  // token: '',
};

const mapStateToProps = (state) => ({
  loading: state.dashboard.loading,
  cards: state.dashboard.cards,
  token: state.authorization.token,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: () => dispatch(fetchDataAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
