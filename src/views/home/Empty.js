import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Background } from '../../components';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function empty() {
  return (
    <Background>
      <View style={styles.container}>
        <Text>Empty screen for DEV purposes!</Text>
      </View>
    </Background>
  );
}
