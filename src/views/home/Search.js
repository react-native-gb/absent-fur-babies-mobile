import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Background } from '../../components';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default function Search() {
  return (
    <Background>
      <View style={styles.container}>
        <Text>Welcome to the search screen</Text>
      </View>
    </Background>
  );
}
