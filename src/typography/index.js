import Heading from './Heading';
import SubHeading from './SubHeading';
import Content from './Content';

export {
  Heading,
  Content,
  SubHeading
};
