import React from 'react';
import {
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const Content = (props) => {
  const { children, theme, style } = props;
  return (
    <Text
      style={[
        {
          fontSize: theme.fontSizeContext,
          fontFamily: theme.fontRegular,
          color: theme.quad100,
        },
        style
      ]}
    >
      {children}
    </Text>
  );
};

Content.propTypes = {
  children: PropTypes.node.isRequired,
  theme: PropTypes.shape({}).isRequired,
  style: PropTypes.shape({})
};

Content.defaultProps = {
  style: {}
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(Content);
