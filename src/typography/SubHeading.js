import React from 'react';
import {
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const SubHeading = (props) => {
  const { children, theme, style } = props;
  return (
    <Text
      style={[{
        fontSize: theme.fontSizeSubHeading,
        fontFamily: theme.fontRegular,
        color: theme.white,
      }, style]}
    >
      {children}
    </Text>
  );
};

SubHeading.propTypes = {
  children: PropTypes.node.isRequired,
  theme: PropTypes.shape({}).isRequired,
  style: PropTypes.shape({})
};

SubHeading.defaultProps = {
  style: {}
};
const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(SubHeading);
