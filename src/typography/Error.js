import React from 'react';
import {
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const Error = (props) => {
  const { children, theme, style } = props;
  return (
    <Text
      style={[
        {
          fontSize: theme.fontSizeError,
          fontFamily: theme.fontRegular,
          color: theme.error,
        },
        style
      ]}
    >
      {children}
    </Text>
  );
};

Error.propTypes = {
  children: PropTypes.node.isRequired,
  theme: PropTypes.shape({}).isRequired,
  styles: PropTypes.shape({})
};

Error.defaultProps = {
  styles: {}
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(Error);
