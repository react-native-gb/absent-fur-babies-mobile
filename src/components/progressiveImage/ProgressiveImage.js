import React, { useEffect } from 'react';

import {
  View, StyleSheet, Animated, Dimensions
} from 'react-native';

import { themeObject } from '../../theme/options';

const d = Dimensions.get('window');

const styles = StyleSheet.create({
  imageOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
  container: {
    backgroundColor: themeObject.athensGrey,
  },
  image: {
    width: d.width - 35,
    height: d.height * 0.30,
  }
});

const thumbnailAnimated = new Animated.Value(0);
const imageAnimated = new Animated.Value(0);

const onThumbnailLoad = () => Animated.timing(thumbnailAnimated, { toValue: 1 }).start();
const onImageLoad = () => Animated.timing(imageAnimated, { toValue: 1 }).start();

const ProgressiveImage = (props) => {
  const {
    thumbnailSource,
    imageSource
  } = props;

  useEffect(() => {
    thumbnailAnimated.setValue(0);
    imageAnimated.setValue(0);
  });

  return (
    <View style={styles.container}>
      <Animated.Image
        source={thumbnailSource}
        onLoad={onThumbnailLoad}
        blurRadius={1}
        style={styles.image}
      />
      <Animated.Image
        source={imageSource}
        style={[styles.imageOverlay, styles.image]}
        onLoad={onImageLoad}
      />
    </View>
  );
};

export default ProgressiveImage;
