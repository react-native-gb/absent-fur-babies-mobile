import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Switch } from 'react-native-paper';
import { connect } from 'react-redux';
import { Content } from '../../typography';

const ToggleWithLabel = (props) => {
  const { onChange, label, theme } = props;
  const [isSwitchOn, setSwitch] = useState(false);

  useEffect(() => {
    onChange(isSwitchOn);
  }, [isSwitchOn]);

  return (
    <SafeAreaView style={styles.container}>
      <Switch
        value={isSwitchOn}
        onValueChange={() => setSwitch(!isSwitchOn)}
        color={theme.quad100}
      />
      <Content style={styles.label}>
        {label}
      </Content>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    marginLeft: 5,
  }
});
ToggleWithLabel.propTypes = {
  onChange: PropTypes.func,
  label: PropTypes.string.isRequired,
};

ToggleWithLabel.defaultProps = {
  onChange: () => null,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(ToggleWithLabel);
