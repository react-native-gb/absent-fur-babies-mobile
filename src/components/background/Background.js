import React from 'react';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo-linear-gradient';
import {
  SafeAreaView, ScrollView, Dimensions, Platform, KeyboardAvoidingView, NativeModules
} from 'react-native';

import { TabBarHeight } from '../../navigation/contants';

const { StatusBarManager } = NativeModules;

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
const screenHeight = Dimensions.get('window').height;

const Background = (props) => {
  const { children, containerStyle } = props;

  return (
    <SafeAreaView>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        <ScrollView
          contentContainerStyle={containerStyle.height ? containerStyle : { height: screenHeight + STATUSBAR_HEIGHT - TabBarHeight }}
          nestedScrollEnabled
        >
          <LinearGradient
            colors={['#C9D8EB', '#ADC5E0', '#92B1D6', '#779ECC']}
            start={[0.0, 0.0]}
            end={[1.0, 1.0]}
            style={{
              flex: 1,
            }}
          >
            {children}
          </LinearGradient>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

Background.propTypes = {
  children: PropTypes.node.isRequired,
  containerStyle: PropTypes.shape({})
};

Background.defaultProps = {
  containerStyle: {}
};

export default Background;
