import * as React from 'react';
import PropTypes from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { View } from 'react-native';

const SettingsIcon = (props) => {
  const { size, color } = props;
  return (
    <View
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <MaterialIcons
        name="settings"
        size={size}
        color={color}
      />
    </View>
  );
};

SettingsIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

SettingsIcon.defaultProps = {
  size: 32,
  color: '#000',
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(SettingsIcon);
