import * as React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';

const Password = (props) => {
  const { size, color } = props;

  return (
    <View
      style={{
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <MaterialIcons
        name="security"
        size={size}
        color={color}
      />
    </View>

  );
};

Password.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

Password.defaultProps = {
  color: '#fff',
  size: 32,
};

export default Password;
