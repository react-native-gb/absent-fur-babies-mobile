import * as React from 'react';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import IconWrapper from './Wrapper';

const Avatar = (props) => {
  const { size, color, theme } = props;
  return (
    <IconWrapper size={size} borderColor={theme.quad75}>
      <Ionicons
        name="md-person-add"
        size={size}
        color={color}
      />
    </IconWrapper>
  );
};

Avatar.propTypes = {
  size: PropTypes.number,
  theme: PropTypes.shape({}).isRequired
};

Avatar.defaultProps = {
  size: 32,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(Avatar);
