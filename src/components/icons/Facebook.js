import * as React from 'react';
import PropTypes from 'prop-types';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from 'react-redux';
import IconWrapper from './Wrapper';

const FacebookIcon = (props) => {
  const { size, theme } = props;
  return (
    <IconWrapper size={size} borderColor={theme.quad75}>
      <FontAwesome name="facebook-f" size={size} color={theme.white} />
    </IconWrapper>
  );
};

FacebookIcon.propTypes = {
  size: PropTypes.number,
  theme: PropTypes.shape({}).isRequired
};

FacebookIcon.defaultProps = {
  size: 32,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(FacebookIcon);
