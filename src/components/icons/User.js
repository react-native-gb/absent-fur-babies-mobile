import * as React from 'react';
import PropTypes from 'prop-types';
import { FontAwesome } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { View } from 'react-native';

const UserIcon = (props) => {
  const { size, color } = props;
  return (
    <View
      style={{
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <FontAwesome
        name="user"
        size={size}
        color={color}
      />
    </View>
  );
};

UserIcon.propTypes = {
  size: PropTypes.number,
  theme: PropTypes.shape({}).isRequired
};

UserIcon.defaultProps = {
  size: 32,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(UserIcon);
