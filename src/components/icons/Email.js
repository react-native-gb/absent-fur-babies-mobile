import * as React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';

const EmailIcon = (props) => {
  const { size, color } = props;

  return (
    <View
      style={{
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <MaterialIcons
        name="email"
        size={size}
        color={color}
      />
    </View>

  );
};

EmailIcon.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

EmailIcon.defaultProps = {
  color: '#fff',
  size: 32,
};

export default EmailIcon;
