import * as React from 'react';
import PropTypes from 'prop-types';
import { SimpleLineIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { View } from 'react-native';

const LogoutIcon = (props) => {
  const { size, color } = props;
  return (
    <View
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <SimpleLineIcons
        name="logout"
        size={size}
        color={color}
      />
    </View>
  );
};

LogoutIcon.propTypes = {
  size: PropTypes.number,
  theme: PropTypes.shape({}).isRequired
};

LogoutIcon.defaultProps = {
  size: 32,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(LogoutIcon);
