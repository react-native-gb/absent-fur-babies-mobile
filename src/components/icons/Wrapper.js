import * as React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';


const IconWrapper = (props) => {
  const { children, size, borderColor } = props;

  return (
    <TouchableOpacity style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: size * 2,
      height: size * 2,
      borderRadius: size * 2,
      backgroundColor: 'transparent',
      borderColor,
      borderWidth: 2,
      marginLeft: 10,
      marginRight: 10,
    }}
    >
      {children}
    </TouchableOpacity>
  );
};

IconWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.number,
  borderColor: PropTypes.string,
};

IconWrapper.defaultProps = {
  size: 32,
  borderColor: '#000'
};

export default IconWrapper;
