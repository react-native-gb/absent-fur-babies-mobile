import * as React from 'react';
import PropTypes from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { View } from 'react-native';

const DashboardIcon = (props) => {
  const { size, color } = props;
  return (
    <View
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <MaterialIcons
        name="dashboard"
        size={size}
        color={color}
      />
    </View>
  );
};

DashboardIcon.propTypes = {
  size: PropTypes.number,
  theme: PropTypes.shape({}).isRequired
};

DashboardIcon.defaultProps = {
  size: 32,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(DashboardIcon);
