import * as React from 'react';
import { SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';


const IconRow = (props) => {
  const { children } = props;

  return (
    <SafeAreaView style={{
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    }}
    >
      {children}
    </SafeAreaView>
  );
};

IconRow.propTypes = {
  children: PropTypes.node.isRequired,
};

export default IconRow;
