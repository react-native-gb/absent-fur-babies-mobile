import * as React from 'react';
import PropTypes from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native';

const MenuIcon = (props) => {
  const { size, color, onPress } = props;
  return (
    <TouchableOpacity
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={() => onPress()}
    >
      <MaterialIcons
        name="menu"
        size={size}
        color={color}
      />
    </TouchableOpacity>
  );
};

MenuIcon.propTypes = {
  size: PropTypes.number,
  // theme: PropTypes.shape({}).isRequired,
  onPress: PropTypes.func,
};

MenuIcon.defaultProps = {
  size: 45,
  onPress: () => null,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(MenuIcon);
