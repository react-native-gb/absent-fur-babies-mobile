import React from 'react';
import LottieView from 'lottie-react-native';
import PropTypes from 'prop-types';

const Spinner = (props) => {
  const { onAnimationFinish, style } = props;
  return (
    <LottieView
      style={style}
      // eslint-disable-next-line global-require
      source={require('./line.json')}
      onAnimationFinish={() => onAnimationFinish()}
      autoPlay
      loop
      enableMergePathsAndroidForKitKatAndAbove
    />
  );
};

Spinner.propTypes = {
  onAnimationFinish: PropTypes.func,
  style: PropTypes.shape({})
};

Spinner.defaultProps = {
  onAnimationFinish: () => null,
  style: {}
};

export default Spinner;
