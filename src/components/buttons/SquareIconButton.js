import React from 'react';
import {
  View, StyleSheet,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Content } from '../../typography';

const SquareIconButton = (props) => {
  const {
    onPress, children, theme, icon, style, isActive
  } = props;

  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={onPress}
    >
      <View style={[styles.iconWrapper, {
        opacity: isActive ? 1 : 0.6,
      }]}
      >
        {icon}
      </View>
      <Content
        style={{
          opacity: isActive ? 1 : 0.6,
          color: theme.white,
        }}
      >
        {children}
      </Content>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    height: '13%',
    width: '100%',
    textAlign: 'center',
    flexDirection: 'row'
  },
  iconWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  }
});

SquareIconButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  labelStyle: PropTypes.shape({}),
  buttonStyle: PropTypes.shape({}),
  theme: PropTypes.shape({}).isRequired,
  icon: PropTypes.node.isRequired,
  style: PropTypes.shape({}),
  isActive: PropTypes.bool,
};

SquareIconButton.defaultProps = {
  buttonStyle: null,
  labelStyle: null,
  style: {},
  isActive: true,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(SquareIconButton);
