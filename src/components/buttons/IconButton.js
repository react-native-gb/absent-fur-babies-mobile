import React from 'react';
import {
  View, StyleSheet,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const IconButton = (props) => {
  const {
    onPress, icon, style, isActive
  } = props;

  return (
    <TouchableOpacity
      style={[styles.container, style]}
      onPress={onPress}
    >
      <View style={[styles.iconWrapper, {
        opacity: isActive ? 1 : 0.6,
      }]}
      >
        {icon}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
  },
  iconWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15
  }
});

IconButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.node.isRequired,
  style: PropTypes.shape({}),
  isActive: PropTypes.bool,
};

IconButton.defaultProps = {
  style: {},
  isActive: true,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(IconButton);
