import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Content } from '../../typography';

const TextButton = (props) => {
  const {
    onPress, children, theme
  } = props;

  return (
    <TouchableOpacity
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        height: '13%',
        width: '100%',
        textAlign: 'center',
      }}
      onPress={onPress}
    >
      <Content
        style={{
          fontSize: theme.fontSizeButton,
          fontFamily: theme.fontRegular,
          color: theme.white,
        }}
      >
        {children}
      </Content>
    </TouchableOpacity>
  );
};

TextButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  labelStyle: PropTypes.shape({}),
  buttonStyle: PropTypes.shape({}),
  theme: PropTypes.shape({}).isRequired
};

TextButton.defaultProps = {
  buttonStyle: null,
  labelStyle: null,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(TextButton);
