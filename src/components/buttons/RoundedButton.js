import React, { useRef, useEffect, useState } from 'react';
import {
  Animated, TouchableOpacity, Easing, StyleSheet, Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Spinner from '../spinner/Spinner';


const screenWidth = Math.round(Dimensions.get('window').width);
const width = Math.round(screenWidth * 0.6);
const height = 55;

const RoundedButton = (props) => {
  const {
    onPress, children, theme, disabled, loading, error
  } = props;

  const labelOpacity = useRef(new Animated.Value(1)).current;
  const loaderOpacity = useRef(new Animated.Value(0)).current;
  const errorOpacity = useRef(new Animated.Value(0)).current;
  const viewWidth = useRef(new Animated.Value(1)).current;

  const [isReductionDone, setReductionState] = useState(false);

  useEffect(() => {
    if (isReductionDone && !loading) {
      Animated.sequence([
        expandViewWidth(),
        decreaseLoaderOpacity(),
        increaseLabelOpacity(),
      ]).start(() => {
        setReductionState(false);
        if (error !== '') {
          increaseErrorOpacity().start();
        }
      });
    }
  }, [loading, isReductionDone]);

  const decreaseLabelOpacity = () => Animated.timing(
    labelOpacity,
    {
      toValue: 0,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );

  const increaseLabelOpacity = () => Animated.timing(
    labelOpacity,
    {
      toValue: 1,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );

  const increaseErrorOpacity = () => Animated.timing(
    errorOpacity,
    {
      toValue: 1,
      duration: 25,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );

  const decreaseErrorOpacity = () => Animated.timing(
    errorOpacity,
    {
      toValue: 0,
      duration: 50,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );
  const increaseLoaderOpacity = () => Animated.timing(
    loaderOpacity,
    {
      toValue: 1,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );

  const decreaseLoaderOpacity = () => Animated.timing(
    loaderOpacity,
    {
      toValue: 0,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true
    }
  );

  const reduceViewWidth = () => Animated.timing(viewWidth, {
    toValue: 0,
    duration: 300,
    easing: Easing.circle,
    useNativeDriver: true,
  });

  const expandViewWidth = () => Animated.timing(viewWidth, {
    toValue: 1,
    duration: 300,
    easing: Easing.circle,
    useNativeDriver: true,
  });

  const handleOnPress = () => {
    if (error !== '') {
      decreaseErrorOpacity().start();
    }

    Animated.sequence([
      decreaseLabelOpacity(),
      increaseLoaderOpacity(),
      reduceViewWidth(),
    ]).start(() => {
      setReductionState(true);
    });
    onPress();
  };

  return (
    <>
      <Animated.View
        style={[
          styles.wrapper,
          {
            transform: [
              {
                scaleX: viewWidth.interpolate({
                  // Input range always as to ascend hence the output range has to be inversed.
                  // as in the end value is 10% of the original and the start value is 100% [0.1, 1]
                  inputRange: [0, 1],
                  outputRange: [0.3, 1]
                })
              }
            ],
            backgroundColor: disabled ? theme.greyTransparent : theme.whiteTransparent,
            paddingBottom: 30,
          }]}
      >
        <TouchableOpacity
          // disabled={disabled || (isExpansionDone || isReductionDone)}
          style={styles.button}
          onPress={handleOnPress}
        >
          <Animated.View
            style={[
              styles.spinnerWrapper, {
                opacity: loaderOpacity,
              }]}
          >
            <Spinner style={styles.spinner} />
          </Animated.View>
          <Animated.Text
            style={[styles.label, {
              fontSize: theme.fontSizeButton,
              fontFamily: theme.fontRegular,
              color: theme.white,
              opacity: labelOpacity,
            }]}
          >
            {error !== '' ? 'Try Again?' : children}
          </Animated.Text>
        </TouchableOpacity>
      </Animated.View>
      <Animated.Text style={{
        marginBottom: 10,
        paddingTop: 8,
        fontSize: theme.fontSizeError,
        fontFamily: theme.fontRegular,
        color: theme.error,
        opacity: errorOpacity,
      }}
      >
        {error}
      </Animated.Text>
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    width,
    // marginBottom: 30,
    height,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    // alignItems: 'center',
    justifyContent: 'center',
  },
  spinnerWrapper: {
    zIndex: 1,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinner: {
    // height,
    width: 60,
    // aspectRatio: 1,
  },
  button: {
    height,
    position: 'absolute',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    zIndex: 0,
    position: 'absolute',
  }
});

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(RoundedButton);
