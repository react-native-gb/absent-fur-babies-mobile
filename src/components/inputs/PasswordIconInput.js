import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native-paper';
import {
  StyleSheet, SafeAreaView, View
} from 'react-native';
import { connect } from 'react-redux';
import PasswordIcon from '../icons/Password';
import { Validate } from '../../validation/validator';
import { PASSWORD } from '../../validation/fields';
import Error from '../../typography/Error';

const styles = StyleSheet.create({
  input: {
    flex: 0.75,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    height: 40,
    paddingLeft: -10,
    // margin: 10,
  },
  icon: {
    flex: 0.15,
    height: 40,
    color: '#000'
  },
  inputContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 8,
  },
  container: {
    width: '75%',
    display: 'flex',
    flexDirection: 'column',
    paddingBottom: 22,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

class PasswordIconInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      errorMessage: '',
      error: false,
    };
  }

  render() {
    const {
      value,
      onTextChange,
      selectionColor,
      mode,
      disabled,
      placeholder,
      theme,
      isValid
    } = this.props;

    const color = theme.grey50;
    const textColor = theme.white;
    const focusColor = theme.white;
    const placeholderColor = theme.grey50;
    const errorColor = theme.error;

    const { isFocused, error, errorMessage } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.inputContainer}>
          <View style={styles.icon}>
            {/* eslint-disable-next-line no-nested-ternary */}
            <PasswordIcon color={isFocused ? textColor : error ? errorColor : color} />
          </View>
          <TextInput
            autoCompleteType="password"
            style={[
              styles.input, {
                fontSize: theme.fontSizeContext,
                fontFamily: theme.fontRegular
              }
            ]}
            error={error}
            secureTextEntry
            placeholder={placeholder}
            disabled={disabled}
            mode={mode}
            underlineColor={color}
            selectionColor={selectionColor}
            value={value}
            onChangeText={(text) => onTextChange(text)}
            theme={{
              colors: {
                // eslint-disable-next-line no-nested-ternary
                text: isFocused ? textColor : error ? errorColor : color,
                primary: focusColor,
                placeholder: error ? errorColor : placeholderColor,
                error: errorColor
              }
            }}
            onFocus={() => this.setState({
              isFocused: true,
              errorMessage: '',
              error: false,
            })}
            onBlur={() => {
              const validator = Validate({ field: PASSWORD, value });
              isValid(validator.isValid);
              if (!validator.isValid) {
                this.setState({
                  isFocused: false,
                  errorMessage: validator.message,
                  error: true,
                });
              }
              this.setState({ isFocused: false });
            }}
          />
        </View>

        {error
          && <Error>{errorMessage}</Error>}
      </SafeAreaView>
    );
  }
}

PasswordIconInput.propTypes = {
  isValid: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  style: PropTypes.shape({}),
  selectionColor: PropTypes.string,
  mode: PropTypes.oneOf(['flat', 'outlined']),
  disabled: PropTypes.bool,
  onTextChange: PropTypes.func,
  theme: PropTypes.shape({}).isRequired
};

PasswordIconInput.defaultProps = {
  isValid: () => null,
  onTextChange: () => null,
  style: {},
  value: null,
  placeholder: null,
  selectionColor: '#000',
  mode: 'flat',
  disabled: false,
};


const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(PasswordIconInput);
