import * as React from 'react';
import {
  StyleSheet, TouchableOpacity, View
} from 'react-native';
import { Divider } from 'react-native-paper';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Content, Heading } from '../../typography';
import SquareIconButton from '../buttons/SquareIconButton';
import Background from '../background/Background';
import Avatar from '../icons/Avatar';
import LogoutIcon from '../icons/Logout';
import { DASHBOARD_ROUTE } from '../../navigation/routeNames';
import DashboardIcon from '../icons/Dashboard';

const DrawerContent = (props) => {
  const { theme } = props;

  const handleIsActive = (name) => {
    if (props.state.routeNames.length === 0) {
      return false;
    }
    return props.state.routeNames[props.state.index] === name;
  };

  return (
    <Background
      style={styles.container}
      containerStyle={{
        height: '100%'
      }}
    >
      <View style={styles.header}>
        <Avatar size={80} />
        <Heading>Hi Gareth</Heading>
        <Content>test@email.com</Content>
      </View>

      <View style={styles.body}>
        <SquareIconButton
          style={{ marginTop: 0 }}
          onPress={() => props.navigation.navigate(DASHBOARD_ROUTE)}
          isActive={handleIsActive(DASHBOARD_ROUTE)}
          icon={<DashboardIcon size={32} color={theme.white} />}
        >
          {DASHBOARD_ROUTE}
        </SquareIconButton>
        <SquareIconButton
          style={{ marginTop: 0 }}
          onPress={() => null}
          icon={<LogoutIcon size={32} color={theme.white} />}
          isActive={handleIsActive('')}
        >
          Menu Item 2
        </SquareIconButton>
      </View>

      <View style={styles.footer}>
        <Content
          style={{
            paddingLeft: 12
          }}
        >
          Themes
        </Content>
        <Divider style={{ backgroundColor: theme.grey100, marginTop: '2%' }} />
        <View
          style={styles.themeIconsWrapper}
        >
          <TouchableOpacity
            style={[styles.themeIcon, {
              backgroundColor: theme.pri50
            }]}
            onPress={() => null}
          >
            <Content
              style={{
                fontSize: theme.fontSizeButton,
                fontFamily: theme.fontRegular,
                color: theme.white,
              }}
            >
              Light
            </Content>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.themeIcon, {
              backgroundColor: '#abab98'
            }]}
            onPress={() => null}
          >
            <Content
              style={{
                fontSize: theme.fontSizeButton,
                fontFamily: theme.fontRegular,
                color: theme.white,
              }}
            >
              Dark
            </Content>
          </TouchableOpacity>
        </View>
        <Divider style={{ backgroundColor: theme.grey100, marginTop: '2%' }} />
        <SquareIconButton
          style={{
            backgroundColor: theme.quad75,
          }}
          onPress={() => null}
          icon={<LogoutIcon size={32} color={theme.white} />}
        >
          Logout
        </SquareIconButton>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    // alignItems: 'stretch',
    // justifyContent: 'center',
    // display: 'flex',
    // flexDirection: 'column',
    // paddingTop: 75,
    // flex: 1,
  },
  themeIconsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingLeft: 15,
  },
  themeIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    height: 55,
    width: 55,
    borderRadius: 20,
    textAlign: 'center',
    marginRight: 15,

  },
  footer: {
    display: 'flex',
    flex: 0.3,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  header: {
    paddingTop: 60,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 0.3
  },
  body: {
    flex: 0.4,
  }
});

SquareIconButton.propTypes = {
  // theme: PropTypes.shape({}).isRequired,
};

SquareIconButton.defaultProps = {
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(DrawerContent);
