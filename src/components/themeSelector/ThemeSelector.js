import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { Content, SubHeading } from '../../typography';


const ThemeSelector = (props) => {
  const { theme, onPress } = props;

  return (
    <View style={styles.container}>
      <SubHeading
        style={{
          paddingLeft: 12
        }}
      >
        Themes:
      </SubHeading>
      <View
        style={styles.themeIconsWrapper}
      >
        <TouchableOpacity
          style={[styles.themeIcon, {
            backgroundColor: theme.pri50
          }]}
          onPress={() => null}
        >
          <Content
            style={{
              fontSize: theme.fontSizeButton,
              fontFamily: theme.fontRegular,
              color: theme.white,
            }}
          >
            Light
          </Content>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.themeIcon, {
            backgroundColor: '#abab98'
          }]}
          onPress={onPress}
        >
          <Content
            style={{
              fontSize: theme.fontSizeButton,
              fontFamily: theme.fontRegular,
              color: theme.white,
            }}
          >
            Dark
          </Content>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    width: '100%',
  },
  themeIconsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  themeIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: 5,
    height: '40%',
    width: '40%',
    borderRadius: 20,
    textAlign: 'center',
    // marginRight: 30,
  }
});

ThemeSelector.propTypes = {
  onPress: PropTypes.func.isRequired,
  theme: PropTypes.shape({}).isRequired,
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(ThemeSelector);
