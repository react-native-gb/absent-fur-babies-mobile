import React, { Fragment } from 'react';

import AdvertCard from './AdvertCard';

import * as cardTypes from '../constants/CardTypes';

const cardChooser = (props) => {
  const {
    type,
    ...data
  } = props;

  switch (type) {
    case cardTypes.ADVERT_CARD: return <AdvertCard {...data} />;
    default: return <></>;
  }
};

export default cardChooser;
