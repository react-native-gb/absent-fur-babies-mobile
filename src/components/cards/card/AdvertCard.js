import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  Paragraph
} from 'react-native-paper';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProgressiveImage from '../../progressiveImage/ProgressiveImage';

import * as advertStatus from '../constants/AdvertStatus';
import { SubHeading, Content } from '../../../typography';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: 8,
    paddingRight: 8,
    marginTop: 12,
    marginBottom: 12,
  },
  headingWrapper: {
    flex: 0.1,
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 8,
  },
  imageWrapper: {
    margin: 8,
    flex: 0.6
  },
  bodyWrapper: {
    flex: 0.3,
    paddingTop: 8,
    paddingBottom: 8,
  },
  contentWrapper: {
    paddingLeft: 8,
  }
});

const advertCard = (props) => {
  const {
    name,
    species,
    breed,
    colour,
    status,
    contactDetails,
    comments,
    thumbnailSource,
    imageSource,
    theme
  } = props;

  // const tags = [species, breed, colour];

  return (
    <View style={styles.container}>
      <View style={{ ...styles.headingWrapper, backgroundColor: theme.quad75 }}>
        <SubHeading>
          {`Name: ${name}`}
        </SubHeading>
      </View>
      <View style={{ ...styles.bodyWrapper, backgroundColor: theme.sec50 }}>
        <View style={styles.contentWrapper}>
          <View style={styles.imageWrapper}>
            <ProgressiveImage
              thumbnailSource={{ uri: thumbnailSource }}
              imageSource={{ uri: imageSource }}
            />
          </View>
          <Content style={{ color: theme.white }}>{(status === advertStatus.LOST ? 'Hero: ' : 'Owner: ') + contactDetails.name}</Content>
          <Content style={{ color: theme.white }}>{`Comments: ${comments}`}</Content>
          <Content style={{ color: theme.white }}>{`Phone: ${contactDetails.number}`}</Content>
          <Content style={{ color: theme.white }}>{`Email: ${contactDetails.email}`}</Content>
        </View>
      </View>
    </View>
  );
};

advertCard.propTypes = {
  theme: PropTypes.shape({}).isRequired,
  name: PropTypes.string,
  species: PropTypes.string,
  breed: PropTypes.string,
  colour: PropTypes.string,
  status: PropTypes.oneOf([advertStatus.LOST, advertStatus.FOUND]),
  contactDetails: PropTypes.shape({
    name: PropTypes.string,
    number: PropTypes.string,
    email: PropTypes.string,
  }),
  comments: PropTypes.string,
  thumbnailSource: PropTypes.string,
  imageSource: PropTypes.string
};

advertCard.defaultProps = {
  name: 'Unknown',
  species: 'Unknown',
  breed: 'Unknown',
  colour: 'Unknown',
  comments: 'Unknown',
  thumbnailSource: 'Unknown',
  imageSource: 'Unknown',
  status: 'Unknown',
  contactDetails: {
    name: 'Unknown',
    number: 'Unknown',
    email: 'Unknown',
  }
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps, null)(advertCard);
