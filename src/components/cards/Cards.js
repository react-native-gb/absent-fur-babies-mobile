import React from 'react';

import CardChooser from './card/CardChooser';

export default (props) => {
  const {
    type,
    items
  } = props;

  const cards = items.map((item) => (
    <CardChooser
      key={item.id}
      type={type}
      {...item}
    />
  ));

  return cards;
};
