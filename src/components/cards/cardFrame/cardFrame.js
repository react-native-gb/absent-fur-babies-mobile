import React from 'react';

import { View, StyleSheet } from 'react-native';

import { themeObject } from '../../../theme/options';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    margin: 10,
    shadowColor: themeObject.codGrey,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }
});

export default (props) => <View style={styles.container}>{props.children}</View>;
