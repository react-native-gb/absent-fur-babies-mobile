import React from 'react';

import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 2,
    paddingHorizontal: 8,
  }
});

export default (props) => <View style={styles.container}>{props.children}</View>;
