import { themes } from '../../../theme/options';
import {
  CHANGE_THEME_SUCCESS,
  CHANGE_THEME_ERROR,
  CHANGE_THEME_STARTED
} from './types';

const INITIAL_STATE = { ...themes, loading: null };

const theme = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_THEME_STARTED:
      return { ...state, loading: true };
    case CHANGE_THEME_SUCCESS:
      return { ...state, theme: action.theme, loading: false };
    case CHANGE_THEME_ERROR:
      // TODO: @Gareth
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default theme;
