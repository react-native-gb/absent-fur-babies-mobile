import { AsyncStorage } from 'react-native';

import { themes } from '../../../theme/options';
import { themeStore } from '../../../theme/constants';
import {
  CHANGE_THEME_SUCCESS,
  CHANGE_THEME_STARTED,
  CHANGE_THEME_ERROR
} from './types';

const storeData = async (name, value) => {
  const dataValue = JSON.stringify(value);
  try {
    await AsyncStorage.setItem(name, dataValue);
  } catch (error) {
    throw new Error(error.message);
  }
};

const themeChangerSuccess = (theme) => ({
  type: CHANGE_THEME_SUCCESS,
  theme: themes[theme],
});

export const themeChangerStartedAction = () => ({
  type: CHANGE_THEME_STARTED,
});

const themeChangerError = (error) => ({
  type: CHANGE_THEME_ERROR,
  error
});

export const themeChangerAction = (theme) => (dispatch) => {
  storeData(themeStore, theme)
    .then(() => dispatch(themeChangerSuccess(theme)))
    .catch((error) => dispatch(themeChangerError(error)));
};
