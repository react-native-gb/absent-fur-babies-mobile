export const CHANGE_THEME_SUCCESS = 'change_theme_success';
export const CHANGE_THEME_STARTED = 'change_theme_started';
export const CHANGE_THEME_ERROR = 'change_theme_error';
