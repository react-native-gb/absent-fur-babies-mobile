export const LOGIN_ERROR = 'login_error';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_STARTED = 'login_started';

export const REGISTER_ERROR = 'register_error';
export const REGISTER_SUCCESS = 'register_success';
export const REGISTER_STARTED = 'register_started';

export const LOGOUT_ERROR = 'logout_error';
export const LOGOUT_SUCCESS = 'logout_success';
export const LOGOUT_STARTED = 'logout_started';
