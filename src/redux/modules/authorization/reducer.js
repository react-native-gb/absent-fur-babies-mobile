import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_STARTED,
  REGISTER_SUCCESS,
  REGISTER_STARTED,
  REGISTER_ERROR, LOGOUT_SUCCESS
} from './types';

const INITIAL_STATE = {
  user: {},
  loading: {
    login: null,
    register: null
  },
  token: null,
  errors: {
    login: '',
    register: ''
  },
};

const authorization = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_STARTED:
      return {
        ...state,
        loading: { ...state.loading, login: true },
        errors: { ...state.errors, login: '' },
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        loading: { ...state, loading: { ...state.loading, login: false } },
        errors: { ...state.errors, login: '' },
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loading: { ...state, loading: { ...state.loading, login: false } },
        errors: { ...state.errors, login: action.error }
      };
    case REGISTER_STARTED:
      return {
        ...state,
        loading: { ...state.loading, register: true },
        errors: { ...state.errors, register: '' }
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        errors: { ...state.errors, register: '' },
        loading: { ...state.loading, register: false }
      };
    case REGISTER_ERROR:
      return {
        ...state,
        loading: { ...state.loading, register: false },
        errors: { ...state.errors, register: action.error }
      };

    case LOGOUT_SUCCESS:
      return {
        ...state,
        user: {},
        loading: {
          login: null,
          register: null
        },
        token: null,
        errors: {
          login: '',
          register: ''
        },
      };
    default:
      return state;
  }
};

export default authorization;
