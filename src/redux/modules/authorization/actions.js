import { AsyncStorage } from 'react-native';
import {
  LOGIN_ERROR,
  LOGIN_STARTED,
  LOGIN_SUCCESS, LOGOUT_SUCCESS,
  REGISTER_ERROR,
  REGISTER_STARTED,
  REGISTER_SUCCESS
} from './types';
import {
  login as loginService,
  register as registerService
} from '../../../services/authorization';
import errors from '../../../services/errors';
import { HOME_ROUTE, LOGIN_ROUTE } from '../../../navigation/routeNames';

const storeToken = async (token) => {
  try {
    await AsyncStorage.setItem('token', JSON.stringify(token));
  } catch (error) {
    throw new Error(error.message);
  }
};

const clearAsyncStorage = async () => {
  await AsyncStorage.removeItem('token');
};

const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

export const logoutAction = ({ navigation }) => (dispatch) => {
  clearAsyncStorage().then(() => {
    navigation.navigate(LOGIN_ROUTE);
    dispatch(logoutSuccess);
  });
};

const loginStarted = () => ({
  type: LOGIN_STARTED
});

const loginError = (error) => ({
  type: LOGIN_ERROR,
  error
});

const loginSuccess = ({ payload, navigation }) => ({
  type: LOGIN_SUCCESS,
  payload,
  navigation
});

export const loginAction = ({
  email, password, navigation, persistLogin
}) => (dispatch) => {
  dispatch(loginStarted());
  loginService({ email, password })
    .then((response) => {
      if (persistLogin) {
        storeToken(response.data.token)
          .then(() => {
            dispatch(loginSuccess({ payload: response.data, navigation }));
            navigation.navigate(HOME_ROUTE);
          })
          .catch((error) => dispatch(loginError(error.message)));
      } else {
        dispatch(loginSuccess({ payload: response.data, navigation }));
        navigation.navigate(HOME_ROUTE);
      }
    })
    .catch((error) => {
      const message = errors(error.response.status);
      dispatch(loginError(message));
    });
};

const registerStarted = () => ({
  type: REGISTER_STARTED
});

const registerError = (error) => ({
  type: REGISTER_ERROR,
  error
});

const registerSuccess = ({ payload, navigation }) => ({
  type: REGISTER_SUCCESS,
  payload,
  navigation
});

export const registerAction = ({
  name, email, password, navigation
}) => (dispatch) => {
  dispatch(registerStarted());
  registerService({ name, email, password })
    .then((response) => {
      dispatch(registerSuccess({ payload: response.data, navigation }));
      navigation.navigate(HOME_ROUTE);
    })
    .catch((error) => {
      const message = errors(error.response.status);
      dispatch(registerError(message));
    });
};
