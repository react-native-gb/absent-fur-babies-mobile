import { dashboardData } from '../../../services/fetch_data';
import {
  FETCH_DATA_ERROR,
  FETCH_DATA_STARTED,
  FETCH_DATA_SUCCESS
} from './types';


const fetchDataStarted = () => ({
  type: FETCH_DATA_STARTED
});

const fetchDataSuccess = (cards) => ({
  type: FETCH_DATA_SUCCESS,
  cards
});

const fetchDataError = (error) => ({
  type: FETCH_DATA_ERROR,
  error
});

const fetchDataAction = () => (dispatch) => {
  dispatch(fetchDataStarted());
  dashboardData()
    .then((cards) => {
      dispatch(fetchDataSuccess(cards));
    })
    .catch((error) => {
      dispatch(fetchDataError(error));
    });
};

// eslint-disable-next-line import/prefer-default-export
export { fetchDataAction };
