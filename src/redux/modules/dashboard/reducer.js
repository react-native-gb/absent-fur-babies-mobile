import {
  FETCH_DATA_ERROR,
  FETCH_DATA_STARTED,
  FETCH_DATA_SUCCESS
} from './types';

const INITIAL_STATE = {
  loading: false,
  cards: [],
  error: null
};

const dashboard = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_DATA_STARTED:
      return {
        ...state,
        loading: true,
      };
    case FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        cards: [...state.cards, ...action.cards]
      };
    case FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default: return state;
  }
};

export default dashboard;
