export const FETCH_DATA_STARTED = 'fetch_data_started';
export const FETCH_DATA_SUCCESS = 'fetch_data_success';
export const FETCH_DATA_ERROR = 'fetch_data_error';
