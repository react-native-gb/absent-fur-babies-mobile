import { combineReducers } from 'redux';
import authorization from '../modules/authorization/reducer';
import dashboard from '../modules/dashboard/reducer';
import theme from '../modules/theme/reducer';

export default combineReducers({
  theme,
  authorization,
  dashboard
});
