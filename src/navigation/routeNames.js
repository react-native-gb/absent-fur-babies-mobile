export const DASHBOARD_ROUTE = 'Dashboard';
export const HOME_ROUTE = 'Home';
export const LOGIN_ROUTE = 'Login';
export const REGISTRATION_ROUTE = 'Registration';
export const SEARCH_ROUTE = 'Search';
export const EMPTY_ROUTE = 'Empty';
export const SETTINGS_ROUTE = 'Settings';
