import HomeTab from './home';
import LoginTab from './login';
import RegisterTab from './register';

export {
  HomeTab,
  LoginTab,
  RegisterTab
};
