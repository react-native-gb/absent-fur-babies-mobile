import React from 'react';
import { Text } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { DASHBOARD_ROUTE, LOGIN_ROUTE } from '../routeNames';
import { activeTab } from '../../theme/constants';
import Tab from './tab';
import Dashboard from '../../views/dashboard/Dashboard';
import { Login } from '../../views/autherization';
import { TabBarHeight } from '../contants';


const LoginTab = (props) => {
  const { theme } = props;
  return (
    <Tab.Navigator
      initialRouteName={LOGIN_ROUTE}
      tabBarOptions={{
        activeTintColor: activeTab,
        style: { height: TabBarHeight }
      }}
    >
      <Tab.Screen
        name={DASHBOARD_ROUTE}
        component={Dashboard}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Dashboard</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="view-dashboard" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={LOGIN_ROUTE}
        component={Login}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Login</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="account" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};


const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps)(LoginTab);
