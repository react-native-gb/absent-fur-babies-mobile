import React from 'react';
import { Text } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { DASHBOARD_ROUTE, REGISTRATION_ROUTE } from '../routeNames';
import { activeTab } from '../../theme/constants';
import Tab from './tab';
import Dashboard from '../../views/dashboard/Dashboard';
import { Registration } from '../../views/autherization';
import { TabBarHeight } from '../contants';

const RegisterTab = (props) => {
  const { theme } = props;
  return (
    <Tab.Navigator
      initialRouteName={DASHBOARD_ROUTE}
      tabBarOptions={{
        activeTintColor: activeTab,
        style: { height: TabBarHeight }
      }}
    >
      <Tab.Screen
        name={DASHBOARD_ROUTE}
        component={Dashboard}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Dashboard</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="view-dashboard" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={REGISTRATION_ROUTE}
        component={Registration}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Register</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="account" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps)(RegisterTab);
