import React from 'react';
import { Text } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import {
  DASHBOARD_ROUTE, EMPTY_ROUTE, SEARCH_ROUTE, SETTINGS_ROUTE
} from '../routeNames';
import { activeTab } from '../../theme/constants';
import Tab from './tab';
import Dashboard from '../../views/dashboard/Dashboard';
import { Empty, Search } from '../../views/home';
import { TabBarHeight } from '../contants';
import Settings from '../../views/settings/Settings';

const HomeTab = (props) => {
  const { theme } = props;
  return (
    <Tab.Navigator
      initialRouteName={DASHBOARD_ROUTE}
      tabBarOptions={{
        activeTintColor: activeTab,
        style: { height: TabBarHeight }
      }}
    >
      <Tab.Screen
        name={DASHBOARD_ROUTE}
        component={Dashboard}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Dashboard</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="view-dashboard" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={SEARCH_ROUTE}
        component={Search}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Search</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="magnify" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={EMPTY_ROUTE}
        component={Empty}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Empty</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="cellphone-screenshot" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={SETTINGS_ROUTE}
        component={Settings}
        options={{
          // eslint-disable-next-line max-len
          tabBarLabel: ({ focused, color: labelColor }) => <Text style={{ color: focused ? theme.quad100 : labelColor }}>Settings</Text>,
          tabBarIcon: ({ color: iconColor, size, focused }) => (
            <MaterialCommunityIcons name="settings" color={focused ? theme.quad100 : iconColor} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const mapStateToProps = (state) => ({
  theme: state.theme.theme,
});

export default connect(mapStateToProps)(HomeTab);
