import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Dashboard from '../../views/dashboard/Dashboard';
import { DrawerContent } from '../../components';
import { DASHBOARD_ROUTE } from '../routeNames';

const Drawer = createDrawerNavigator();


export default function DrawerNavigator() {
  return (
    <>
      <Drawer.Navigator
        initialRouteName={DASHBOARD_ROUTE}
        drawerContent={(props) => <DrawerContent {...props} />}
      >
        <Drawer.Screen name={DASHBOARD_ROUTE} component={Dashboard} />
      </Drawer.Navigator>
    </>
  );
}
