import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeTab, LoginTab, RegisterTab } from '../tabs';
import {
  HOME_ROUTE,
  LOGIN_ROUTE,
  REGISTRATION_ROUTE,
  SETTINGS_ROUTE
} from '../routeNames';
import Settings from '../../views/settings/Settings';

const Stack = createStackNavigator();

export default function StackNavigator(props) {
  const { isPersistedToken } = props;
  return (
    <>
      <Stack.Navigator
        initialRouteName={isPersistedToken ? HOME_ROUTE : REGISTRATION_ROUTE}
        screenOptions={{
          headerShown: false
        }}
      >
        <Stack.Screen
          name={HOME_ROUTE}
          component={HomeTab}
        />
        <Stack.Screen
          name={LOGIN_ROUTE}
          component={LoginTab}
        />
        <Stack.Screen
          name={REGISTRATION_ROUTE}
          component={RegisterTab}
        />
        <Stack.Screen
          name={SETTINGS_ROUTE}
          component={Settings}
        />
      </Stack.Navigator>
    </>
  );
}
