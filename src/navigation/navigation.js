import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { AppLoading } from 'expo';

import StackNavigator from './stack/navigator';
import { primary, themeStore } from '../theme/constants';

// Actions
import {
  themeChangerAction,
  themeChangerStartedAction
} from '../redux/modules/theme/actions';

class Navigation extends Component {
  // eslint-disable-next-line react/state-in-constructor
  state = {
    isPersistedToken: false,
    persistedTokenLoading: false,
  };

  componentDidMount() {
    const { changeTheme, changeThemeStarted } = this.props;
    changeThemeStarted();
    AsyncStorage.getItem(themeStore, (theme) => {
      if (theme) {
        changeTheme(theme);
      } else {
        changeTheme(primary);
      }
    });

    AsyncStorage.getItem('token', (token) => {
      if (token) {
        this.setState({
          isPersistedToken: true,
          persistedTokenLoading: false,
        });
      }
    });
  }

  render() {
    const {
      fontsLoaded, themeLoading
    } = this.props;

    const { persistedTokenLoading, isPersistedToken } = this.state;

    if (!fontsLoaded || themeLoading || persistedTokenLoading) {
      return (
        <AppLoading />
      );
    }

    return (
      <NavigationContainer>
        <StackNavigator isPersistedToken={isPersistedToken} />
      </NavigationContainer>
    );
  }
}

StackNavigator.propTypes = {
  changeTheme: PropTypes.func,
  themeLoading: PropTypes.bool,
  fontsLoaded: PropTypes.bool,
  changeThemeStarted: PropTypes.func,
};

StackNavigator.defaultProps = {
  themeLoading: false,
};

const mapDispatchToProps = (dispatch) => ({
  changeTheme: (theme) => dispatch(themeChangerAction(theme)),
  changeThemeStarted: () => dispatch(themeChangerStartedAction()),
});

const mapStateToProps = (state) => ({
  themeLoading: state.theme.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
