import React from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { useFonts } from '@use-expo/font';
import EStyleSheet from 'react-native-extended-stylesheet';

import initialStore from './src/redux/store';
import Navigation from './src/navigation/navigation';
import {
  montserratRegular,
  montserratMedium,
  montserratBold,
} from './src/theme/constants';

EStyleSheet.build({ // always call EStyleSheet.build() even if you don't use global variables!
  $textColor: '#0275d8'
});

const store = initialStore();


export default function App() {
  const [fontsLoaded] = useFonts({
    // eslint-disable-next-line global-require
    [montserratRegular]: require('./assets/fonts/Montserrat-Regular.ttf'),
    // eslint-disable-next-line global-require
    [montserratMedium]: require('./assets/fonts/Montserrat-Medium.ttf'),
    // eslint-disable-next-line global-require
    [montserratBold]: require('./assets/fonts/Montserrat-Bold.ttf'),
  });

  return (
    <Provider store={store}>
      <PaperProvider>
        <Navigation fontsLoaded={fontsLoaded} />
      </PaperProvider>
    </Provider>
  );
}
