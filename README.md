# ABSENT FUR BABIES MOBILE

## Developer Setup

### Requirements
- NodeJs: https://nodejs.org/en/download/
- Expo CLI: https://docs.expo.io/versions/latest/get-started/installation/
- Jetbrains Webstorm or any other sub par IDE.
- Expo mobile application

### Tech
- expo
- react-native paper https://callstack.github.io/react-native-paper/index.html
- react-navigation
- redux, redux-thunk, redux-promise

### Application Startup
#### Developement
You may need to run:
```shell script
$ npm i -g opencollective
```
this is a problem with react navigation
```sh
$ expo start
```
Scan the bar code and the app will open on your mobile device.

### Debugging
For more information check out https://medium.com/@hmajid2301/debugging-expo-apps-in-webstorm-and-visual-studio-code-8b7457958173

### Dependencies 
It might might habit or impulse to you yarn or npm but please use expo install


